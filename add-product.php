<?php 

require_once "DB-connect.php";
require_once "product.php";
require_once "DVD.php";
require_once "furniture.php";
require_once "book.php";

$errors = [];
//declaring empty product for correct display on first load
$array = (array) new Product();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    
    $productType = $_POST['product-type'];
    $product = new $productType();
    $product -> setSKU();
    $product -> setName();
    $product -> setPrice();
    $product -> setType($productType);
    $product -> setParameters();
    $array = (array) $product;
    
//checking if product with same 'sku' already exists in DB
    $duplicate = $pdo -> prepare('SELECT sku FROM products WHERE sku = :sku');
    $duplicate -> execute([
        ':sku' => $product -> getSKU()
    ]);    
    $duplicateProduct = $duplicate -> fetch(PDO::FETCH_ASSOC);
    
//generating display strings for each possible error (missing field or duplicate SKU)
    if (!$array['sku']) {
        $errors[] = 'missing "SKU"';
    } else if ($duplicateProduct && $array['sku'] == $duplicateProduct['sku']) {
        $errors[] = 'Duplicate SKU';
    }

    if (!$array['name']) {
        $errors[] = 'missing "name"';
    }
    
    if (!$array['price']) {
        $errors[] = 'Missing or Wrong value for "Price"';
    }

    if ($product -> missingParameters($array)) {
        $errors[] = 'all product-unique parameters are required [numeric values]';
    }

    if (empty($errors)) {
        $product -> setDescription();

        $statement = $pdo->prepare("INSERT INTO products (sku, name, price, type, description)
        VALUES(:sku, :name, :price, :type, :description)");

        $statement->execute([
            ':sku' => $product -> getSKU(),
            ':name' => $product -> getName(),
            ':price' => $product -> getPrice(),
            ':type' => $productType,
            ':description' => $product -> getDescription()
        ]);
        
        header('Location: index.php');
    }
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Product Add</title>
</head>
<body>    
    <header>
        <h1 id="product-list">Product Add</h1>

<!-- displaying errors related to missing fields / SKU match -->
        <?php if (!empty($errors)) :?>

            <div class="">
                <span class="alert">
                    <h4>ERRORS!!! | 
                        <?php foreach ($errors as $error): ?>
                            <?php echo $error . ' | ' ?>
                        <?php endforeach; ?>
                    </h4>          
                </span>                
            </div>
        <?php endif; ?>
    </header>
    
    <div id="main-table">
        <form action="" method="post" id="product_form">
            <div id="buttons" class="buttons">
                <button type="submit" class="disabled" id="save-button">Save</button>
                <a href="index.php" class="button">Cancel</a>
            </div>
            <div>
                <div class="title">
                    <label for="sku" class="col-form-label">SKU</label>
                </div>
                <div class="input-value">
                    <input type="text" name="sku" id="sku" class="input-value" value="<?php echo $array['sku'] ?>">
                </div>
            </div>
            <div>
                <div class="title">
                    <label for="name" class="col-form-label">Name</label>
                </div>
                <div class="input-value">
                    <input type="text" name="name" id="name" class="input-value" value="<?php echo $array['name'] ?>">
                </div>
            </div>
            <div>
                <div class="title">
                    <label for="price" class="col-form-label">Price ($)</label>
                </div>
                <div class="input-value">
                    <input type="text" name="price" id="price" class="input-value" value="<?php echo $array['price'] ?>">
                </div>
            </div>

            <div>
                <label for="product-type" class="type-switcher-title">Type Switcher</label>
<!-- using JS function 'val()' to handle dynamic display of product unique fields -->
                <select class="type-switcher" name="product-type" id="productType" onchange="val()">
                    <option value="" selected hidden>Type Switcher</option>
                    <option value="DVD">DVD</option>
                    <option value="Furniture">Furniture</option>
                    <option value="Book">Book</option>
                </select>
                             
            </div>
<!-- <div> 'details-fields' is being populated based on JS function 'val()' -->
            <div class="input-value" id="details-fields"></div>
        </form>        
        
    </div>

    <footer>
        Scandiweb Test Assignment
    </footer>
    
</body>
<script src="myscripts.js"></script>
</html>