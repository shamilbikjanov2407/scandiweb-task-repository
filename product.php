<?php 
class Product {
//using 'inheritance' - fields 'sku', 'name', 'price' and 'description' 
//are initialized in Parent class 'Product' for child classes to use

    public $sku = '';
    public $name = '';
    public $price = '';
    public $type = null;
    public $description = null;

//no constcruction is required, since all objects are initiated with 'null' fields
    public function __construct() {}

    public function setSKU() {
        $this -> sku = $_POST['sku'];
    }

    public function setName() {
        $this -> name = $_POST['name'];
    }

    public function setPrice() {
        $this -> price = $this -> setParameter('price');
    }

    public function setType($type) {
        $this -> type = $type;
    }

    public function setDescription() {
        $this -> description = 'description';
    }

    public function getSKU() {
        return $this -> sku;
    }

    public function getName() {
        return $this -> name;
    }

    public function getPrice() {
        return $this -> price;
    }

    public function getDescription() {
        return $this -> description;
    }

    public function setParameters() {

    }

//custom function 'setParameter' helps avoid code-duplication when validating
// a) if 'comma' was used instead of 'dot' for decials
// b) if user entered a non-numerical value for any of the parameters
    public function setParameter($parameter) {
        $temp = $_POST[$parameter];
        $temp = str_replace(',', '.', $temp);
        
        if (is_numeric($temp)) {
            return $temp;
        }
        return '';
    }
    
//'missingParameters' function is used to check whether all type-unique parameters are entered
//same function is being overridden for every child-class of 'Product' class
    public function missingParameters($array) {}
}

?>