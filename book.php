<?php

require_once "product.php";

class Book extends Product {
    public $weight = null;
    public $label = '(KG)';
    public $memo = '*Please provide weight in kilograms';

    public function __construct() {
        parent::__construct();
    }

    public function setDescription() {
        $this -> description = 'Weight: '. round($this -> weight) . ' KG';
    }
    
    public function setParameters() {
        $this -> weight = parent::setParameter('weight');
    }
    
    public function missingParameters($array) {
        if (!$array['weight']) {
            return true;
        }
        return false;     
    }

    public function getWeight() {
        return $this -> weight;
    }
}