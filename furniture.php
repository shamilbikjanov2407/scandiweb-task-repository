<?php

require_once "product.php";

class Furniture extends Product {
    public $height = null;
    public $width = null;
    public $length = null;    
    public $label = '(CM)';
    public $memo = "*Please provide dimensions in H <b><em>x</em></b> W <b><em>x</em></b> L format in centimeters";


    public function __construct() {
        parent::__construct();
    }

    public function setDescription() {
        $this -> description = 'Dimension: '. round($this -> height) . ' x ' . 
                                round($this -> width) . ' x ' . round($this -> length);
    }

    public function setParameters() {
        $this -> height = parent::setParameter('height');
        $this -> width = parent::setParameter('width');
        $this -> length = parent::setParameter('length');
    }

    public function getHeight() {
        return $this -> height;
    }

    public function getWidth() {
        return $this -> width;
    }

    public function getLength() {
        return $this -> length;
    }
    
    public function missingParameters($array) {
        if (!$array['height'] || !$array['width'] || !$array['length']) {
            return true;
        }
        return false;     
    }
}