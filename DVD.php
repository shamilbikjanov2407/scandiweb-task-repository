<?php

require_once "product.php";

class DVD extends Product {
    public $size = null;
    public $label = '(MB)';
    public $memo = '*Please provide size in megabytes';

    public function __construct() {
        parent::__construct();
    }

    public function setDescription() {
        $this -> description = 'Size: '. round($this -> size) . ' MB';
    }

    public function setParameters() {
        $this -> size = parent::setParameter('size');
    }

    public function getSize() {
        return $this -> size;
    }

//'missingParameters' function is used to check whether all type-unique parameters are entered
//same function is being overridden for every child-class of 'Product' class
    public function missingParameters($array) {
        if (!$array['size']) {
            return true;
        }
        return false;     
    }
}