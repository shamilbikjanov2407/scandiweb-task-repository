<?php 
require_once "DB-connect.php";
require_once "product.php";
require_once "DVD.php";
require_once "furniture.php";
require_once "book.php";

$statement = $pdo -> prepare('SELECT * FROM products ORDER BY sku ASC');
$statement  -> execute();
$products = $statement -> fetchAll(PDO::FETCH_ASSOC);

// checked_sku[] array - holds all 'sku' numbers for products, 
// where on index.php page checkboxed were selected
if (!empty($_POST['checked_sku'])) {

    foreach ($_POST['checked_sku'] as $sku) {
        $delete = $pdo -> prepare('DELETE FROM products WHERE sku = :sku');
        $delete -> bindValue(':sku', $sku);
        $delete -> execute();

        header("Location: index.php");
    }
    $pdo = null;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Product List</title>
</head>
<body>    
    <header>
        <h1 id="product-list">Product List</h1>
        
    </header>
    
    <div id="main-table">

        <div>
            <a href="add-product.php" class="button buttons" style="right: 150px;">ADD</a>
            <form style="display: inline-block;" name="bulk-action-form" method="post">
                <input type="hidden" name="mass-delete-submit" value="">
                <button type="sumbit" class="button buttons" id="delete-product-btn">MASS DELETE</button>

<!-- using foreach loop to display products -->
                <?php foreach($products as $product): ?>
                    <div class="sale-item">
                        <input class="delete-checkbox" type="checkbox" id="checkbox" name="checked_sku[]" value="<?php echo $product['sku'] ?>">
                        <p class="item-code">
                            <?php echo $product['sku'] ?>
                        </p>
                        <p class="item-name">
                            <?php echo $product['name'] ?>
                        </p>
                        <p class="item-price">
                            <?php echo number_format($product['price'], 2, '.', ' ') ?> $
                        </p>
                        <p class="item-details">
                            <?php echo $product['description'] ?>
                        </p>
                    </div>                        
                <?php endforeach; ?>  

            </form>
        </div>

              
    </div>

    <footer>
        Scandiweb Test Assignment
    </footer>

    
</body>
<script src="myscripts.js"></script>
</html>