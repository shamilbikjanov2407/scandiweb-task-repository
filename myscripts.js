function Product() {
    this.label = "";
    this.memo = "";
}

function DVD() {
    Product.call(this);
    this.size = "";
    this.label = '(MB)';
    this.memo = '*Please provide size in megabytes';
}

DVD.prototype = Object.create(Product.prototype);
DVD.prototype.constructor = DVD;

function Furniture() {
    Product.call(this);
    this.height = "";
    this.width = "";
    this.length = "";
    this.label = '(CM)';
    this.memo = "*Please provide dimensions in H x W x L format in centimeters";
}

Furniture.prototype = Object.create(Product.prototype);
Furniture.prototype.constructor = Furniture;

function Book() {
    Product.call(this);
    this.weight = "";
    this.label = '(KG)';
    this.memo = '*Please provide weight in kilograms';
}

Book.prototype = Object.create(Product.prototype);
Book.prototype.constructor = Book;

//using if-else on this occasion only, since I have no idea how to do this otherwise. Sorry.
function val() {
    typeSelected = document.getElementById("productType").value;
    if (typeSelected == "Book") {
        var product = new Book;
    } else if (typeSelected == "DVD") {
        var product = new DVD;
    } else {
        var product = new Furniture;
    }

    document.getElementById("details-fields").innerHTML = "";
    var parentDiv = document.getElementById("details-fields");

    for (var key in product) {
        if (key == 'size' || key == 'weight' || key == 'height' || key == 'length' || key == 'width') {
            
            var div = document.createElement("DIV");
            var subDiv = document.createElement("DIV");
            subDiv.setAttribute("class", "title");
            var label = document.createElement("LABEL");
            var labelText = document.createTextNode(key + " " + product['label']);
            label.setAttribute("for", "price");
            label.setAttribute("class", "col-form-label");
                
            var subDiv2 = document.createElement("DIV");
            subDiv2.setAttribute("class", "input-value")
            var input = document.createElement("INPUT");
            input.setAttribute("type", "text");
            input.setAttribute("name", key);
            input.setAttribute("id", key);
            input.setAttribute("class", "input-value");
        
            subDiv2.appendChild(input);
            label.appendChild(labelText);
            subDiv.appendChild(label);
            div.appendChild(subDiv);
            div.appendChild(subDiv2);    
            parentDiv.appendChild(div);
        }        
    }
        
    var memoText = document.createTextNode(product['memo']);
    var memo = document.createElement("DIV");
    memo.setAttribute("class", "memo");
    memo.appendChild(memoText);
    parentDiv.appendChild(memo);
}

document.getElementById("productType").addEventListener("change", enableSaveButton);
disableSaveButton();

function enableSaveButton() {
    document.getElementById("save-button").disabled = false;
    document.getElementById("save-button").removeAttribute('class');
    document.getElementById("save-button").setAttribute('class', 'button');
    document.getElementById("save-button").removeEventListener("mousedown", emptyFieldAlert);
}

function disableSaveButton() {
    document.getElementById("save-button").removeAttribute('class');
    document.getElementById("save-button").setAttribute('class', 'disabled');
    document.getElementById("save-button").addEventListener("mousedown", emptyFieldAlert);
}

function emptyFieldAlert() {
    alert("Please choose type of product");
}